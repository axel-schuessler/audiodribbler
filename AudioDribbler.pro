#-------------------------------------------------
#
# Project created by QtCreator 2011-11-02T17:28:42
#
#-------------------------------------------------

QT += core gui widgets

TARGET = AudioDribbler
TEMPLATE = app

INCLUDEPATH += src

SOURCES += src/main.cpp \
   src/FootControlHandler.cpp \
   src/MainWindow.cpp \
   src/MPlayerWrapper.cpp

HEADERS += src/FootControlHandler.h \
   src/MainWindow.h \
   src/MPlayerWrapper.h

FORMS += ui/MainWindow.ui
