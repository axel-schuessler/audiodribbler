#include <QFileDialog>
#include <QSettings>
#include <QCloseEvent>

#include "MainWindow.h"
#include "ui_MainWindow.h"


#include <iostream>
using std::cout;

MainWindow::MainWindow(QString deviceName, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    device(deviceName)
{
    ui->setupUi(this);
    connect(ui->fileSelectButton, SIGNAL(clicked()), this, SLOT(openAudioFile()));

    footControlHandler = new FootControlHandler(device, this);
    ui->deviceLabel->setText(QString("%1 on %2").arg(footControlHandler->getDeviceName())
                             .arg(footControlHandler->getDevicePath()));

    mplayerWrapper = new MplayerWrapper(this);

    connect(footControlHandler, SIGNAL(rightPedalPressed()), mplayerWrapper, SLOT(play()));
    connect(footControlHandler, SIGNAL(rightPedalReleased()), mplayerWrapper, SLOT(pause()));
    connect(footControlHandler, SIGNAL(leftPedalPressed()), mplayerWrapper, SLOT(rewindStart()));
    connect(footControlHandler, SIGNAL(leftPedalReleased()), mplayerWrapper, SLOT(rewindStop()));
    connect(footControlHandler, SIGNAL(middlePedalPressed()), mplayerWrapper, SLOT(fastForwardStart()));
    connect(footControlHandler, SIGNAL(middlePedalReleased()), mplayerWrapper, SLOT(fastForwardStop()));

    connect(mplayerWrapper, SIGNAL(timePositionChanged(float,float)), this, SLOT(updateTimeValues(float,float)));
    connect(ui->progressSlider, SIGNAL(valueChanged(int)), mplayerWrapper, SLOT(seekAbsolute(int)));

    readSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openAudioFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Audio File"),
                                                    "/home/axel", tr("Audio Files (*.mp3 *.wav)"));
    ui->audioFileLabel->setText(fileName);
    mplayerWrapper->loadAudioFile(fileName);
}

void MainWindow::updateTimeValues(float timePos, float totalLength)
{
    /* update progress bar, the bar checks itself if the new value is within range */
    bool wasBlocked = ui->progressSlider->blockSignals(true);
    ui->progressSlider->setValue((int)(timePos * 100 / totalLength));
    ui->progressSlider->blockSignals(wasBlocked); // either unblock or keep blocked if it was kept blocked before someplace else <-- defensive coding
    QString timeDisplay = QString("%1:%2 / %3:%4").arg((int)(timePos/60), 2, 10, QChar('0'))
            .arg((int)timePos % 60, 2, 10, QChar('0'))
            .arg((int)(totalLength/60), 2, 10, QChar('0'))
            .arg((int)totalLength % 60, 2, 10, QChar('0'));
    ui->timePosLabel->setText(timeDisplay);
}


const QString s_audioFileName = "audioFileName";
const QString s_audioLastPosition = "lastPosition";

void MainWindow::readSettings()
{
    QSettings settings("Ax", "AudioDribbler");
    /* load last audio file */
    if (settings.contains(s_audioFileName)) {
        QString audioFileName = settings.value(s_audioFileName).toString();
        if (QFile::exists(audioFileName)) {
            ui->audioFileLabel->setText(audioFileName);
            mplayerWrapper->loadAudioFile(audioFileName);
            /* seek to last position */
            if (settings.contains(s_audioLastPosition)) {
                float lastPosition = settings.value(s_audioLastPosition).toFloat();
                if (lastPosition > 3.0f) {
                    lastPosition -= 3.0f; // move position 3 seconds back
                }
                mplayerWrapper->seekAbsolute(lastPosition);
            }
        }
    }

}

void MainWindow::writeSettings()
{
    QSettings settings("Ax", "AudioDribbler");
    /* save filename and current position */
    settings.setValue(s_audioFileName, mplayerWrapper->getAudioFileName());
    settings.setValue(s_audioLastPosition, QString::number(mplayerWrapper->getAudioPosition(), 'f', 2));
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    writeSettings();
    event->accept();
}
