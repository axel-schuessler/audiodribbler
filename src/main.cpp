#include <QApplication>
#include <QStringList>

#include "MainWindow.h"

#include <iostream>
using std::cout;



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString deviceFileName = FootControlHandler::scanBlockDevices("/dev/input");

    MainWindow w(deviceFileName);

    if (a.arguments().size() >= 4) {
        bool ok;
        int seekPos = a.arguments().at(3).toInt(&ok, 10);
        if (!ok) {
            cout << "Invalid seek position." << std::endl;
            return 1;
        }
//        myMplayerWrapper->seekRelativeTo(seekPos);
    }


    w.show();

    return a.exec();
}
