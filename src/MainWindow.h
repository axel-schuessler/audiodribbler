#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>

#include "FootControlHandler.h"
#include "MPlayerWrapper.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QString deviceName, QWidget *parent = 0);
    ~MainWindow();

private slots:
    void openAudioFile();
    void updateTimeValues(float timePos, float totalLength);

private:
    Ui::MainWindow *ui;
    FootControlHandler *footControlHandler;
    MplayerWrapper *mplayerWrapper;

    QString device;

    void readSettings();
    void writeSettings();

protected:
    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
