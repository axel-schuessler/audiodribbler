#include <QProcess>
#include <QTimer>
#include <QTextStream>

#include "MPlayerWrapper.h"

#include <iostream>
using std::cout;


MplayerWrapper::MplayerWrapper(QObject *parent) :
    QObject(parent)
{
    rewindTimer = new QTimer(this);
    rewindTimer->setInterval(100);
    connect(rewindTimer, SIGNAL(timeout()), this, SLOT(rewindClickIncrement()));
    rewindClicks = 0;
    rewinding = true;

    updateWhilePlayingTimer = new QTimer(this);
    updateWhilePlayingTimer->setInterval(1000);
    connect(updateWhilePlayingTimer, SIGNAL(timeout()), this, SLOT(updateTimeValues()));

    mplayer = new QProcess(this);

    currentTimePosition = totalLength = 0.0f;

    audioFileName = "";
    runMplayerSlave();
}

MplayerWrapper::~MplayerWrapper()
{
    mplayer->write("quit\n");
    mplayer->waitForFinished();
}

QString MplayerWrapper::getAudioFileName()
{
    return audioFileName;
}

float MplayerWrapper::getAudioPosition()
{
    return currentTimePosition;
}


/* public slots */

void MplayerWrapper::loadAudioFile(QString fileName)
{
    audioFileName = fileName;
    QString command = "load \"" + fileName + "\"\nloop 1\npause\n";
    mplayer->write(command.toUtf8());
    updateTimeValues();
}

void MplayerWrapper::seekAbsolute(int percent)
{
    if (percent < 0 || percent >= 100) {
        return;
    }
    QString command = QString("pausing_keep seek ") + QString::number(percent) + " 1\n"; // suffix 1 = absolute, percentage
    mplayer->write(command.toUtf8());
    updateTimeValues();
}

void MplayerWrapper::seekAbsolute(float position)
{
    if (position < 0.0f || (totalLength > 0.0f && position >= totalLength)) {
        return;
    }
    QString command = QString("pausing_keep seek ") + QString::number(position, 'f', 2) + " 2\n"; // suffix 2 = absolute, seconds
    mplayer->write(command.toUtf8());
    updateTimeValues();
}


/* audio player controls */
void MplayerWrapper::pause()
{
    /* seek backwards 1 second and pause */
    updateWhilePlayingTimer->stop();
    updateTimeValues();
    mplayer->write("pausing seek -1\n");
}

void MplayerWrapper::play()
{
    /* get arbitrary property to exit pause loop */
    mplayer->write("get_file_name\n");
    updateWhilePlayingTimer->start();
}

void MplayerWrapper::rewindStart()
{
    rewindClicks = 1;
    rewinding = true;
    if (!rewindTimer->isActive()) {
        rewindTimer->start(100);
    }
}

void MplayerWrapper::rewindStop() {
    rewindTimer->stop();
    rewindClicks %= (int)totalLength;
    QString command = "";
    if (rewindClicks > currentTimePosition) {
        rewindClicks -= (int)currentTimePosition;
        currentTimePosition = totalLength - rewindClicks;
        command = QString("pausing_keep seek ") + QString::number(currentTimePosition) + " 2\n";
    } else {
        command = QString("pausing_keep seek -") + QString::number(rewindClicks) + "\n";
    }
    mplayer->write(command.toUtf8());
    updateTimeValues();
}

void MplayerWrapper::fastForwardStart()
{
    rewindClicks = 1;
    rewinding = false;
    if (!rewindTimer->isActive()) {
        rewindTimer->start(100);
    }
}

void MplayerWrapper::fastForwardStop()
{
    rewindTimer->stop();
    QString command = QString("pausing_keep seek ") + QString::number(rewindClicks % (int)totalLength) + "\n";
    mplayer->write(command.toUtf8());
    updateTimeValues();
}

void MplayerWrapper::togglePause()
{
    mplayer->write("pause\n");
}


/* private slots */

void MplayerWrapper::readOutput()
{
    bool valuesChanged = false;
    QByteArray output = mplayer->readAllStandardOutput();
    QTextStream outputStream(output);
    while (!outputStream.atEnd()) {
        QString line = outputStream.readLine();
        //cout << line.toStdString() << std::endl;
/*        if (line.contains("ANS_PERCENT_POSITION=")) {
            bool ok = false;
            int newPos = line.mid(21).toInt(&ok);
            if (ok) {
                emit positionChanged(newPos);
            }
        } else*/ if (line.contains("ANS_LENGTH=")) {
            bool ok = false;
            float totalLengthSeconds = line.mid(11).toFloat(&ok);
            if (ok) {
                totalLength = totalLengthSeconds;
                valuesChanged = true;
            }
        } else if (line.contains("ANS_TIME_POSITION=")) {
            bool ok = false;
            float timePositionSeconds = line.mid(18).toFloat(&ok);
            if (ok) {
                currentTimePosition = timePositionSeconds;
                valuesChanged = true;
            }
        }
    }
    if (valuesChanged) {
        emit timePositionChanged(currentTimePosition, totalLength);
    }
}

void MplayerWrapper::rewindClickIncrement()
{
    rewindClicks++;
    if (rewinding) {
        currentTimePosition -= 1.0f;
    } else {
        currentTimePosition += 1.0f;
    }
    emit timePositionChanged(currentTimePosition, totalLength);
}

void MplayerWrapper::updateTimeValues()
{
    //mplayer->write("pausing_keep get_percent_pos\npausing_keep get_time_length\npausing_keep get_time_pos\n");
    mplayer->write("pausing_keep get_time_length\npausing_keep get_time_pos\n");
}


void MplayerWrapper::runMplayerSlave()
{
    QString program = "mplayer";
    QStringList arguments;
    arguments << "-slave" << "-quiet" << "-idle"; // << "../testing/Bulat.mp3"
    mplayer->start(program, arguments);

    /* this ensures that mplayer output is written to stdout */
    connect(mplayer, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
}
