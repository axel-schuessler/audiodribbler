#ifndef FOOTCONTROLHANDLER_H
#define FOOTCONTROLHANDLER_H

#include <QObject>

QT_BEGIN_NAMESPACE
class QSocketNotifier;
QT_END_NAMESPACE

class FootControlHandler : public QObject
{
    Q_OBJECT

public:
    FootControlHandler(QString devicePath, QObject* parent = 0);
    ~FootControlHandler();

    QString getDevicePath() const { return m_devicePath; }
    QString getDeviceName() const { return m_deviceName; }
    bool isReady() const { return isDeviceOpen; }

    static QString scanBlockDevices(QString path);

public slots:
    void setDevicePath(QString devicePath);

private slots:
    void readEvents(int);

signals:
    void rightPedalPressed();
    void rightPedalReleased();
    void middlePedalPressed();
    void middlePedalReleased();
    void middlePedalRepeat();
    void leftPedalPressed();
    void leftPedalReleased();
    void leftPedalRepeat();

private:
    int openDevice();

    QString m_devicePath;
    QString m_deviceName;
    int fileDescriptor;
    bool isDeviceOpen;

    QSocketNotifier *notifier;

};

#endif // FOOTCONTROLHANDLER_H
