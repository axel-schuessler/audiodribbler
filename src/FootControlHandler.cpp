/*

  FootControlHandler class: opens a specified /dev/input/eventX device and interprets the
  EV_KEY events from a Sony FootControlUnit.
  (passes the file descriptor to a QSocketNotifier, which then watches for new events
   during the main event loop)

  emits signals according to pedals pressed, released or having their repeat function triggered

 */

#include <QSocketNotifier>
#include <QDir>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/input.h>
#include <unistd.h>

#include "FootControlHandler.h"

#include <iostream>
using std::cout;


static const QString footControlName("Sony ICD-FootControlUnit");

FootControlHandler::FootControlHandler(QString devicePath, QObject* parent) : QObject(parent)
{
    m_devicePath = devicePath;
    m_deviceName = "Unknown";
    fileDescriptor = -1;
    isDeviceOpen = false;
    notifier = NULL;

    openDevice();
}

FootControlHandler::~FootControlHandler()
{
    if (isDeviceOpen) {
        //delete notifier; //notifier will be automatically deleted due to qt object tree
        close(fileDescriptor);
    }
}

void FootControlHandler::setDevicePath(QString devicePath) {
    m_devicePath = devicePath;
    openDevice();
}

int FootControlHandler::openDevice()
{
    if (isDeviceOpen) {
        delete notifier;
        close(fileDescriptor);
        isDeviceOpen = false;
    }

    //Open Device
    QByteArray deviceNameBytes = m_devicePath.toUtf8();
    const char* deviceNameChar = deviceNameBytes.data();
    //cout << "Opening " << deviceNameChar << std::endl;
    if ((fileDescriptor = open(deviceNameChar, O_RDONLY)) == -1) {
        cout << "not a valid device.\n" << std::endl;
        fileDescriptor = -1;
        return -1;
    }

    //Print Device Name
    char deviceName[256] = "Unknown";
    if (ioctl(fileDescriptor, EVIOCGNAME (sizeof (deviceName)), deviceName) >= 0) {
        //cout << "Device is " << deviceName << std::endl;
    }
    m_deviceName = QString(deviceName);

    // grab device
    int grab = 1;
    if ((ioctl(fileDescriptor, EVIOCGRAB, &grab)) != 0) {
        cout << "Error obtaining exclusive lock on device" << std::endl;
        close(fileDescriptor);
        return -1;
    }

    notifier = new QSocketNotifier(fileDescriptor, QSocketNotifier::Read, this);
    connect(notifier, SIGNAL(activated(int)), this, SLOT(readEvents(int)));

    isDeviceOpen = true;
    return 0;
}

void FootControlHandler::readEvents(int)
{
    struct input_event ev[64];
    const int size = sizeof(struct input_event); // input_event struct size is 24 bytes

    int read_bytes;
    if ((read_bytes = read(fileDescriptor, ev, size * 64)) < size) {
        cout << "short read()" << std::endl;
        return;
    }

    //int eventCounter = 0;
    for(int evno = 0; evno < (int)(read_bytes / size); evno++) {
        if (ev[evno].type == EV_KEY) { // Only listen to EV_KEY events
            //cout << "(" << ++eventCounter << "-" << evno << ") Key pressed: code " << ev[evno].code << " value " << ev[evno].value << std::endl;
            switch(ev[evno].code) {
            case 68: // Right foot presses
                if (ev[evno].value == 1) {
                    //cout << "Right foot pressed." << std::endl;
                    emit rightPedalPressed();
                }
                break;
            case 87: // Right foot releases
                if (ev[evno].value == 1) {
                    //cout << "Right foot released." << std::endl;
                    emit rightPedalReleased();
                }
                break;
            case 106:
                if (ev[evno].value == 1) { // Middle pedal pressed
                    //cout << "Middle pedal pressed." << std::endl;
                    emit middlePedalPressed();
                } else if (ev[evno].value == 0) { // Middle pedal released
                    //cout << "Middle pedal released." << std::endl;
                    emit middlePedalReleased();
                } else if (ev[evno].value == 2) { // Middle pedal repeat
                    //cout << "Middle pedal sends repeat." << std::endl;
                    emit middlePedalRepeat();
                }
                break;
            case 105:
                if (ev[evno].value == 1) { // Left pedal pressed
                    //cout << "Left pedal pressed." << std::endl;
                    emit leftPedalPressed();
                } else if (ev[evno].value == 0) { // Left pedal released
                    //cout << "Left pedal released." << std::endl;
                    emit leftPedalReleased();
                } else if (ev[evno].value == 2) { // Left pedal repeat
                    //cout << "Left pedal sends repeat." << std::endl;
                    emit leftPedalRepeat();
                }
                break;
            }
        }
    }
}

// from linux/input.h
// we only get 0, 1 and 4 on the FootControlUnit
/*
 * Event types
 *

#define EV_SYN                  0x00 <--
#define EV_KEY                  0x01 <--
#define EV_REL                  0x02
#define EV_ABS                  0x03
#define EV_MSC                  0x04 <--
#define EV_SW                   0x05
#define EV_LED                  0x11
#define EV_SND                  0x12
#define EV_REP                  0x14
#define EV_FF                   0x15
#define EV_PWR                  0x16
#define EV_FF_STATUS            0x17
#define EV_MAX                  0x1f
#define EV_CNT                  (EV_MAX+1)
*/

QString FootControlHandler::scanBlockDevices(QString path)
{
    QDir deviceDirectory(path);
    deviceDirectory.setFilter(QDir::System | QDir::Readable); // "eventX" are system files, and we only want those we can read
    QStringListIterator deviceFileIterator(deviceDirectory.entryList());
    while(deviceFileIterator.hasNext()) {
        QFile deviceFile(deviceDirectory.filePath(deviceFileIterator.next()));
        if (deviceFile.open(QIODevice::ReadOnly)) {
            // when file is opened, read device name
            char deviceName[256] = "Unknown";
            if (ioctl(deviceFile.handle(), EVIOCGNAME (sizeof (deviceName)), deviceName) >= 0) {
                if (footControlName.compare(deviceName) == 0) {
                    cout << "... found " << footControlName.toStdString() << " on " << deviceFile.fileName().toStdString() << std::endl;
                    return deviceFile.fileName();
                }
            }
        }
    }
    return "";
}
