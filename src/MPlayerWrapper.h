#ifndef MPLAYERWRAPPER_H
#define MPLAYERWRAPPER_H

#include <QObject>

QT_BEGIN_NAMESPACE
class QProcess;
class QTimer;
QT_END_NAMESPACE

class MplayerWrapper : public QObject
{
    Q_OBJECT
public:
    explicit MplayerWrapper(QObject *parent = 0);
    ~MplayerWrapper();

    QString getAudioFileName();
    float getAudioPosition();

signals:
    void timePositionChanged(float timePos, float totalTime);

public slots:
    void loadAudioFile(QString fileName);
    void seekAbsolute(int percent);
    void seekAbsolute(float position);

    void pause();
    void play();
    void rewindStart();
    void rewindStop();
    void fastForwardStart();
    void fastForwardStop();
    void togglePause();

private slots:
    void readOutput();
    void rewindClickIncrement();
    void updateTimeValues();

private:
    void runMplayerSlave();

    QString audioFileName;
    QProcess *mplayer;
    QTimer *rewindTimer;
    QTimer *updateWhilePlayingTimer;
    int rewindClicks;
    bool rewinding;
    float currentTimePosition;
    float totalLength;
};

#endif // MPLAYERWRAPPER_H
